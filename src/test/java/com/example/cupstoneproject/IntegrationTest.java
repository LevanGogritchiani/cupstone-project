package com.example.cupstoneproject;

import com.example.cupstoneproject.CupstoneProjectApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = CupstoneProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationTest {


    private int port = 8080;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testShowMyLoginPage() {
        String baseUrl = "http://localhost:" + port;
        String endpoint = "/showMyLoginPage";

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(baseUrl + endpoint, String.class);

        // Expecting an HTTP 200 status code
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        // Add more assertions if needed based on the actual response content or behavior.
    }
}
