package com.example.cupstoneproject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.ui.Model;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import com.example.cupstoneproject.dto.ProductDTO;
import com.example.cupstoneproject.model.Category;
import com.example.cupstoneproject.model.Product;
import com.example.cupstoneproject.service.CategoryService;
import com.example.cupstoneproject.service.ProductService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import com.example.cupstoneproject.controller.AdminController;
import com.example.cupstoneproject.model.Category;
import com.example.cupstoneproject.service.CategoryService;
import com.example.cupstoneproject.service.ProductService;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

public class AdminControllerTest {
    @InjectMocks
    private AdminController adminController;

    @Mock
    private CategoryService categoryService;

    @Mock
    private ProductService productService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdminHome() {
        String result = adminController.adminHome();
        assertEquals("adminHome", result);
    }
    @Test
    public void testProducts() {
        Model model = mock(Model.class);
        String result = adminController.products(model);
        assertEquals("products", result);
    }



    @Test
    public void testGetCat() {
        Model model = mock(Model.class);
        when(categoryService.getAllCategory()).thenReturn(Arrays.asList(new Category()));

        String result = adminController.getCat(model);

        assertEquals("categories", result);
        verify(model, times(1)).addAttribute("categories", categoryService.getAllCategory());
    }

    @Test
    public void testGetCatAdd() {
        Model model = mock(Model.class);

        String result = adminController.gerCatAdd(model);

        assertEquals("categoriesAdd", result);
        verify(model, times(1)).addAttribute("category", new Category());
    }

    @Test
    public void testProductAddGet() {
        // Arrange
        Model model = mock(Model.class);

        // Mock behavior of categoryService.getAllCategory()
        when(categoryService.getAllCategory()).thenReturn(Arrays.asList(new Category()));

        // Act
        String result = adminController.productAddGet(model);

        // Assert
        assertEquals("productsAdd", result);

       }

    @Test
    public void testPostCatAdd() {
        Category category = new Category();
        String result = adminController.postCatAdd(category);

        assertEquals("redirect:/admin/categories", result);
        verify(categoryService, times(1)).addCategory(category);
    }


    @Test
    public void testDeleteCat() {
        int categoryId = 1;
        String result = adminController.deleteCat(categoryId);

        assertEquals("redirect:/admin/categories", result);
        verify(categoryService, times(1)).removeCategoryById(categoryId);
    }

    @Test
    public void testUpdateCat() {
        int categoryId = 1;
        Model model = mock(Model.class);
        Optional<Category> category = Optional.of(new Category());
        when(categoryService.getCategoryById(categoryId)).thenReturn(category);

        String result = adminController.updateCat(categoryId, model);

        assertEquals("categoriesAdd", result);
        verify(model, times(1)).addAttribute("category", category.get());
    }


    @Test
    public void testProductAddPost() throws IOException {
        // Mock data
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(1L);
        productDTO.setName("Test Product");
        productDTO.setCategoryId(1);
        productDTO.setPrice(100.0);
        productDTO.setDescription("Test Description");

        Category category = new Category();
        category.setId(1);
        category.setName("Test Category");

        MultipartFile file = new MockMultipartFile("test-image.jpg", "test-image.jpg", "image/jpeg", new byte[0]);
        String imgName = "test-image.jpg";

        // Mock behavior
        when(categoryService.getCategoryById(productDTO.getCategoryId())).thenReturn(Optional.of(category));

        // Call the method
        String result = adminController.productAddPost(productDTO, file, imgName);

        // Assertions
        assertEquals("redirect:/admin/products", result);

        // Verify interactions with productService
        verify(productService, times(1)).addProduct(any(Product.class));

        // Verify interactions with categoryService
        verify(categoryService, times(1)).getCategoryById(productDTO.getCategoryId());
    }
    @Test
    public void testDeleteProduct() {

        long productId = 1L;

        String result = adminController.deleteProduct(productId);

        assertEquals("redirect:/admin/products", result);
}
    @Test
    public void testUpdateProductGet() {
        // Arrange
        Model model = mock(Model.class);
        long productId = 1L;

        // Mock behavior of productService.getProductById(id)
        Product product = new Product();
        Category category = new Category();
        product.setCategory(category);

        when(productService.getProductById(productId)).thenReturn(Optional.of(product));

        // Act
        String result = adminController.updateProductGet(productId, model);

        // Assert
        assertEquals("productsAdd", result);

      }

}
