package com.example.cupstoneproject;
import com.example.cupstoneproject.controller.CartController;
import com.example.cupstoneproject.global.GlobalCart;
import com.example.cupstoneproject.model.Product;
import com.example.cupstoneproject.service.ProductService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CartControllerTest {

    @Mock
    private ProductService productService;

    @Mock
    private Model model;

    @InjectMocks
    private CartController cartController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testAddToCart() {
        // Arrange
        int productId = 1;

        // Mock behavior of productService.getProductById(id)
        Product product = new Product();
        when(productService.getProductById(productId)).thenReturn(Optional.of(product));

        // Act
        String result = cartController.addToCart(productId);

        // Assert
        assertEquals("redirect:/shop", result);
    }

    @Test
    void testCartGet() {
        // Arrange
        GlobalCart.cart.clear();
        GlobalCart.cart.add(new Product());

        // Act
        String result = cartController.cartGet(model);

        // Assert
        assertEquals("cart", result);}

    @Test
    void testCartItemRemove() {
        // Arrange
        int index = 0;
        GlobalCart.cart.clear();
        GlobalCart.cart.add(new Product());

        // Act
        String result = cartController.cartItemRemove(index);

        // Assert
        assertEquals("redirect:/cart", result);
        assertEquals(0, GlobalCart.cart.size());
    }

    @Test
    void testCheckout() {
        // Arrange
        GlobalCart.cart.clear();
        GlobalCart.cart.add(new Product());

        // Act
        String result = cartController.checkout(model);

        // Assert
        assertEquals("checkout", result); verify(model, times(1)).addAttribute("total", GlobalCart.cart.stream().mapToDouble(Product::getPrice).sum());
    }
}
