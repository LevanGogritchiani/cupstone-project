package com.example.cupstoneproject;

import com.example.cupstoneproject.model.Product;
import com.example.cupstoneproject.repository.ProductRepository;
import com.example.cupstoneproject.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ProductServiceTest {
    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllProduct() {
        List<Product> products = Arrays.asList(new Product(), new Product());
        when(productRepository.findAll()).thenReturn(products);

        List<Product> result = productService.getAllProduct();

        assertEquals(2, result.size());
        verify(productRepository, times(1)).findAll();
    }

    @Test
    public void testAddProduct() {
        Product product = new Product();
        productService.addProduct(product);

        verify(productRepository, times(1)).save(product);
    }

    @Test
    public void testRemoveProductById() {
        long productId = 1;
        productService.removeProductById(productId);

        verify(productRepository, times(1)).deleteById(productId);
    }

    @Test
    public void testGetProductById() {
        long productId = 1;
        Product product = new Product();
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        Optional<Product> result = productService.getProductById(productId);

        assertEquals(product, result.orElse(null));
        verify(productRepository, times(1)).findById(productId);
    }

}