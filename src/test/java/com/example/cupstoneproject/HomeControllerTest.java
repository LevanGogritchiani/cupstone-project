package com.example.cupstoneproject;

import com.example.cupstoneproject.controller.HomeController;
import com.example.cupstoneproject.global.GlobalCart;
import com.example.cupstoneproject.model.Product;
import com.example.cupstoneproject.service.CategoryService;
import com.example.cupstoneproject.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class HomeControllerTest {

    @Mock
    private CategoryService categoryService;

    @Mock
    private ProductService productService;

    @Mock
    private Model model;

    @InjectMocks
    private HomeController homeController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testShop() {
        when(categoryService.getAllCategory()).thenReturn(Collections.emptyList());
        when(productService.getAllProduct()).thenReturn(Collections.emptyList());
        when(model.addAttribute(anyString(), any())).thenReturn(model);

        String viewName = homeController.shop(model);

        assertEquals("shop", viewName);}

    @Test
    void testShopByCategory() {
        // Arrange
        int categoryId = 1;
        when(categoryService.getAllCategory()).thenReturn(Collections.emptyList());
        when(productService.getAllProductsByCategoryId(categoryId)).thenReturn(Collections.emptyList());
        when(model.addAttribute(anyString(), any())).thenReturn(model);

        String viewName = homeController.shopByCaregory(model, categoryId);
        assertEquals("shop", viewName);}


}
