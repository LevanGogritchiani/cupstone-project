package com.example.cupstoneproject;

import com.example.cupstoneproject.controller.RegistrationController;
import com.example.cupstoneproject.model.User;
import com.example.cupstoneproject.service.UserService;
import com.example.cupstoneproject.user.WebUser;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class RegistrationControllerTest {
    @Mock
    private UserService userService;

    @InjectMocks
    private RegistrationController registrationController;

    @Mock
    private Logger logger;

    @Mock
    private Model model;

    @Mock
    private HttpSession session;

    @Mock
    private WebDataBinder dataBinder;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void processRegistrationForm_userAlreadyExists() {
        WebUser webUser = new WebUser();
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(false);

        when(userService.findByUserName(anyString())).thenReturn(new User());

        String viewName = registrationController.processRegistrationForm(
                webUser, bindingResult, session, model);
        assertEquals("/registration-confirmation", viewName);



    }

    @Test
    void processRegistrationForm_withBindingErrors() {
        WebUser webUser = new WebUser();
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = registrationController.processRegistrationForm(
                webUser, bindingResult, session, model);
        assertEquals("/registration-form", viewName);

    }



    @Test
    void processRegistrationForm_success() {
        WebUser webUser = new WebUser();
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(false);
        when(userService.findByUserName(anyString())).thenReturn(null);

        String viewName = registrationController.processRegistrationForm(
                webUser, bindingResult, session, model);

        // Add your assertions based on the expected behavior of the method
        assertEquals("/registration-confirmation", viewName);

    }
}
