package com.example.cupstoneproject;
import com.example.cupstoneproject.model.Category;
import com.example.cupstoneproject.repository.CategoryRepository;
import com.example.cupstoneproject.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CategoryServiceTest {
    @InjectMocks
    private CategoryService categoryService;

    @Mock
    private CategoryRepository categoryRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllCategory() {
        List<Category> categories = Arrays.asList(new Category(), new Category());
        when(categoryRepository.findAll()).thenReturn(categories);

        List<Category> result = categoryService.getAllCategory();

        assertEquals(2, result.size());
        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    public void testAddCategory() {
        Category category = new Category();
        categoryService.addCategory(category);

        verify(categoryRepository, times(1)).save(category);
    }

    @Test
    public void testRemoveCategoryById() {
        int categoryId = 1;
        categoryService.removeCategoryById(categoryId);

        verify(categoryRepository, times(1)).deleteById(categoryId);
    }

    @Test
    public void testGetCategoryById() {
        int categoryId = 1;
        Category category = new Category();
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(category));

        Optional<Category> result = categoryService.getCategoryById(categoryId);

        assertEquals(category, result.orElse(null));
        verify(categoryRepository, times(1)).findById(categoryId);
    }

}
