package com.example.cupstoneproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CupstoneProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CupstoneProjectApplication.class, args);
	}

}
