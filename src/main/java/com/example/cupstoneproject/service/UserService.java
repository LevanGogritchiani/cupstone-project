package com.example.cupstoneproject.service;

import com.example.cupstoneproject.model.User;
import com.example.cupstoneproject.user.WebUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    public User findByUserName(String userName);

    void save(WebUser webUser);

}