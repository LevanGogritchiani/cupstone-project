package com.example.cupstoneproject.controller;

import com.example.cupstoneproject.global.GlobalCart;
import com.example.cupstoneproject.service.CategoryService;
import com.example.cupstoneproject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;



@Controller
public class HomeController {
    @Autowired
    CategoryService categoryService;
    @Autowired
    ProductService productService;
    /*@GetMapping({"/", "/home"})
    public String home(){
        model.addAttribute("cartCount", GlobalCart.cart.size());
        return "home";
    }*/

    @GetMapping({"/","/shop"})
    public String shop(Model model){
        model.addAttribute("categories", categoryService.getAllCategory());
        model.addAttribute("products", productService.getAllProduct());
        model.addAttribute("cartCount", GlobalCart.cart.size());
        return "shop";
    }

    @GetMapping("/shop/category/{id}")
    public String shopByCaregory(Model model, @PathVariable int id){
        model.addAttribute("categories",categoryService.getAllCategory());
        model.addAttribute("products", productService.getAllProductsByCategoryId(id));
        model.addAttribute("cartCount", GlobalCart.cart.size());

        return "shop";
    }
@GetMapping("/shop/viewproduct/{id}")
    public String viewProduct(Model model, @PathVariable int id){
        model.addAttribute("product", productService.getProductById(id).get());
        model.addAttribute("cartCount", GlobalCart.cart.size());

    return "viewProduct";
}



}
