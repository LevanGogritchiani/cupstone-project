package com.example.cupstoneproject.dao;

import com.example.cupstoneproject.model.Role;

public interface RoleDao {

    public Role findRoleByName(String theRoleName);


}
