package com.example.cupstoneproject.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@RequiredArgsConstructor
@Table(name = "role")
public class Role extends BaseEntity {
    @Column(name = "name")
    private String name;
}