    package com.example.cupstoneproject.repository;

    import com.example.cupstoneproject.model.Category;
    import org.springframework.data.jpa.repository.JpaRepository;

    public interface CategoryRepository extends JpaRepository<Category , Integer> {
    }
